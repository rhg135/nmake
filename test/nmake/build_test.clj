(ns nmake.build-test
  (:import [java.util.concurrent ForkJoinTask])
  (:require [clojure.test :refer :all]
            [nmake.build :refer :all]))

(defn- success?
  [r]
  (-> (#{:build/success} r) boolean))

(defn- in-1 [] (Thread/sleep 1000) :build/success)

(deftest make-test
  (testing "handles no dependencies"
    (is (instance? ForkJoinTask (make #{}))))
  (testing "empty input"
    (are [a] (nil? (make a))
         {}
         []))
  (testing "other input"
    (are [a] (nil? (make a))
         0 nil true \a #"")))

(deftest builds
  (testing "linear trees"
    (are [args] (success? (apply build args))
         [[{:id :a.o :build in-1} #{}]]
         [[{:id :a.o :build in-1} #{}] 6]
         [[{:id :b :build in-1} #{{:id :b.o :build in-1}}]]
         [[{:id :b :build in-1} #{{:id :b.o :build in-1}}] 2])))
