(defproject nmake "0.0.1-SNAPSHOT"
  :description "NMake an extremely flexible build tool rooted in simplicity"
  :url "http://bitbucket.org/rhg135/nmake"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]])
