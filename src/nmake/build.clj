(ns nmake.build
  (:import [clojure.lang IPersistentSet IPersistentMap IPersistentVector]
           [java.util.concurrent RecursiveTask ForkJoinPool]))

(defn- success? [r] (= :build/success r))

(defn- join! [task] (.join (.fork task)))

(def log (atom []))
(defn info [& args] (swap! log conj (doall (apply str args))))

(defprotocol Targetable
  "NMake targets"
  (make [this] "Builds the target and returns a status"))

(extend-protocol Targetable
  IPersistentSet
  (make [s]
    (info "make called on " s)
    (proxy [RecursiveTask] []
      (compute []
        (info "Building "  s)
        (let [tasks (for [e s] (make e))
              futures (for [t tasks] (.fork t))
              results (for [f futures] (.get f))]
          (if (every? success? results)
            :build/success
            :build/failure)))))
  IPersistentMap
  (make [{:keys [id build]}]
    (proxy [RecursiveTask] []
      (compute []
        (info "Building " id)
        (build))))
  IPersistentVector
  (make [[tgt deps]]
    (proxy [RecursiveTask] []
      (compute []
        (info "Dependencies: " deps)
        (if (and (success? (join! (make deps))) (success? (join! (make tgt))))
          :build/success
          :build/failure)))))

(defn build
  "Builds a dependency tree"
  ([tree n]
   (.invoke (ForkJoinPool. n) (make tree)))
  ([tree]
   (build tree (+ 2 (.. Runtime getRuntime availableProcessors)))))
