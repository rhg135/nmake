# nmake

An extremely flexible build tool.

## Design

NMake uses a relatively simple design in that all components should be seperate meaning:

	* The builder doesn't depend on a concrete format.
	* The (unwritten) parser is optional.
	* All parts can be optionally replaced

## Contribution

If you contribute please keep it focused.
DO ONE THING AND DO IT WELL.

## Usage

### Build Tree

Eventually I'll write some sort of DSL, but for now:

	[{:id :b :build <some unary fn>} #{{:id :b.o :build <fn>}}]

Keep in mind this is only the default data representation.

#### Status:

Unlike arcane old C based tools, NMake doesn't use integer codes by default.

:build/success -> All Good
:build/failure -> Oh Noez!
:build/... -> What?

#### Step:

	{:id <keyword id> :build <unary fn returning a status>}

#### Dependencies:

	#{[STEP]*}

#### Target:

	[<step> <deps>]


to build said tree:

	(require '[nmake.build :refer [build])
	(build <tree> [jobs: defaults to 2 + cpus])

## License

Copyright © 2014 Ricardo Gomez

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
